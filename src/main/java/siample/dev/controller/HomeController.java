package siample.dev.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@RequestMapping("/user/{id}")
	public String searchForUser(@PathVariable(value = "id") String id, Model model) throws Exception {

		System.out.println("Request received. Requested userid is: " + id);
		if (id == "gina") {
			throw new Exception("User not found"); // EZ A SOR SOSEM FUT LE, MÉG if (id==null) ESETÉN SEM.
		} else {
			model.addAttribute("userid", id.toString());
			return "user";
		}
	}

//	//@ExceptionHandler(value = { Exception.class })
//	@ExceptionHandler(Exception.class)
//	public String exch(HttpServletRequest request, Model model, Exception ex) {
//		String exmsg = ex.getMessage();
//		System.out.println(exmsg);
//		model.addAttribute("exmessage", ex.getMessage());
//		return "exceptionhandler";
//	}

	@RequestMapping("/")
	public String index(Model model, Locale locale) {
		model.addAttribute("titel", "FIFA");

		System.out.println(String.format("Request received. Language: %s, Country: %s %n", locale.getLanguage(),
				locale.getCountry()));
		return "stories";
	}

}

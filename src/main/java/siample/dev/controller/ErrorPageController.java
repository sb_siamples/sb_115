package siample.dev.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.ServletWebRequest;

@Controller
public class ErrorPageController implements ErrorController {

	private static final String ERR_PATH = "/error";
	
	@Autowired
	private ErrorAttributes eA;
	
	@RequestMapping(ERR_PATH)
	public String error(Model model, HttpServletRequest request) {
		
		ServletWebRequest req = new ServletWebRequest(request);
		Map<String,Object> error = this.eA.getErrorAttributes(req, true);
		
		model.addAllAttributes(error);
		
		return "error";
	}
	
	@Override
	public String getErrorPath() {
		// ide kerül, esik be az  összes hibás url meghívás a projektre vonatkozóan.
		return ERR_PATH;
	}

}
